#!/bin/bash

for i in {0..49}
do
  for j in {1..10}
  do
    for k in {1..30}
    do
      echo $i
      echo $j
      echo $k
      echo ---
      python3 /REPO_PATH/testing-stt-robustness/OneImgAnlsys-SolveLISPandas-Spots.py $i $j $k
    done
  done
done
