"""
- INFO:
    This script is designed to solve the LIS problem over an image.
    Then, the idea is to adding white lines over the sample image and study how the success percent of the image
    change when the number of lines increases. Also, the width of the lines increases.
- DATE:
    Creation: M18/04/22
"""
import aux_functions as fn
import pandas as pd
import stt_functions as stt
import sys
import time

REPO_PATH = "/REPO_PATH"
IMG_DIR = "{}/sample_pics".format(REPO_PATH)
SCRIPT_DIR = "{}/testing-stt-robustness".format(REPO_PATH)

if len(sys.argv) == 4:
    IMG_NUMBER = int(sys.argv[1])
    LINE_SIZE = int(sys.argv[2])
    NUMBER_OF_LINES = int(sys.argv[3])
    print(" ---> IMG NUMBER: {}".format(IMG_NUMBER))
    print(" ---> LINE SIZE: {} pix".format(LINE_SIZE))
    print(" ---> EVALUATING IMAGE WITH {} LINES".format(NUMBER_OF_LINES))
else:
    raise ValueError("---> ERROR: Please introduce the argument IMG NUMBER, LINE_SIZE, and_NUMBER_OF_LINES")

img_dir = fn.image_list(IMG_DIR)[IMG_NUMBER]
print(" ---> IMAGE DIRECTORY: {}".format(img_dir))

# Solve LIS adding extra lines and save data.

NUMBER_OF_LIS_EXECUTIONS = 20

tm1 = time.time()
full_data = []
for nmbr_of_exec in range(NUMBER_OF_LIS_EXECUTIONS):
    try:
        print(' ---> Execution number: {} - Size: {} - N lines: {}'.format(nmbr_of_exec + 1, LINE_SIZE,
                                                                           NUMBER_OF_LINES))
        solution = stt.solve_lis(img_dir, NUMBER_OF_LINES, LINE_SIZE, stain_type="lines")
        dt_lis = [1]
        dt_lis.extend(solution)
    except Exception as err:
        dt_lis = fn.zero_data()
        print(" ---> ERROR: {}".format(err))
    full_data.append(dt_lis)
# Compute statistical result
# For LIS mean
status_column = [row[0] for row in full_data]
lis_mean = (sum(status_column) / len(status_column)) * 100
final_result_lis = [lis_mean, 0, 0, 0, 0, 0]
full_data.append(final_result_lis)
# For Nr mean
nr_column = [int(row[5]) for row in full_data if row[0] == 1]
if len(nr_column) == 0:
    nr_mean = 0
else:
    nr_mean = (sum(nr_column) / len(nr_column))
final_result_nr = [nr_mean, 0, 0, 0, 0, 0]
full_data.append(final_result_nr)
# Save data
df = pd.DataFrame(data=full_data)
df.columns = ["STATUS", "RA", "DEC", "Roll", "sig", "Nr"]
fname = "{}/Measurements/pic_{}/lines/s{}_nl{}.csv".format(SCRIPT_DIR, IMG_NUMBER + 1, LINE_SIZE, NUMBER_OF_LINES)
df.to_csv(fname, index=False)
tm2 = time.time()
print("\n ---> TOTAL SCRIPT TIME: {:.3f}\n".format(tm2 - tm1))
sys.exit()
