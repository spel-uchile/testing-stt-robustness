"""
- INFO:
    This script is designed to solve the LIS problem over an image.
    The idea is to remove extracted objects from the sample image and study how the success percent of the image
    change when the number of stars decreases. The stars can be removed in two ways: from brightest stars
    to dimmest and backward.
- DATE:
    Creation: T21/04/22
    Modified: S11/06/22
"""
import aux_functions as fn
import pandas as pd
import stt_functions as stt
import sys
import time

REPO_PATH = "/REPO_PATH"
IMG_DIR = "{}/sample_pics".format(REPO_PATH)
SCRIPT_DIR = "{}/testing-stt-robustness".format(REPO_PATH)
TOTAL_STAR_LEN = 35

if len(sys.argv) == 2:
    IMG_NUMBER = int(sys.argv[1])
    print(" ---> IMG NUMBER: {}".format(IMG_NUMBER))
else:
    raise ValueError("---> ERROR: Please introduce the argument IMG NUMBER")

img_dir = fn.image_list(IMG_DIR)[IMG_NUMBER]
print(" ---> IMAGE DIRECTORY: {}".format(img_dir))

tm1 = time.time()
stt.img2fits_and_sext(img_dir)

full_data_bright = []
for ii in range(1, TOTAL_STAR_LEN + 1):
    try:
        solution = stt.solve_lis_removing_stars(removed_stars=ii, star_type="BRIGHT")
        dt_lis = [1]
        dt_lis.extend(solution)
    except Exception as err:
        dt_lis = fn.zero_data()
        print(" ---> ERROR: {}".format(err))
    full_data_bright.append(dt_lis)

full_data_dim = []
for ii in range(1, TOTAL_STAR_LEN + 1):
    try:
        solution = stt.solve_lis_removing_stars(removed_stars=ii, star_type="DIM")
        dt_lis = [1]
        dt_lis.extend(solution)
    except Exception as err:
        dt_lis = fn.zero_data()
        print(" ---> ERROR: {}".format(err))
    full_data_dim.append(dt_lis)

# Save data
df_b = pd.DataFrame(data=full_data_bright)
df_b.columns = ["STATUS", "RA", "DEC", "Roll", "sig", "Nr"]
fname_b = "{}/Measurements/pic_{}/stars/bright.csv".format(SCRIPT_DIR, IMG_NUMBER + 1)
df_b.to_csv(fname_b, index=False)

df_d = pd.DataFrame(data=full_data_dim)
df_d.columns = ["STATUS", "RA", "DEC", "Roll", "sig", "Nr"]
fname_d = "{}/Measurements/pic_{}/stars/dim.csv".format(SCRIPT_DIR, IMG_NUMBER + 1)
df_d.to_csv(fname_d, index=False)

tm2 = time.time()
print("\n ---> TOTAL SCRIPT TIME: {:.3f}\n".format(tm2 - tm1))
sys.exit()
