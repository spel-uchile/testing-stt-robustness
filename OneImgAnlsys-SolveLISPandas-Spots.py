"""
- INFO:
    This script is designed to solve the LIS problem over an example image.
    Then, the idea is to adding white spots to the sample image and study how the success percent of the image
    change when the number of white spots increase.
- DATE:
    Creation: W16/03/22
"""
import aux_functions as fn
import pandas as pd
import stt_functions as stt
import sys
import time

REPO_PATH = "/REPO_PATH"
IMG_DIR = "{}/sample_pics".format(REPO_PATH)
SCRIPT_DIR = "{}/testing-stt-robustness".format(REPO_PATH)

if len(sys.argv) == 4:
    IMG_NUMBER = int(sys.argv[1])
    SPOT_SIZE = int(sys.argv[2])
    NUMBER_OF_SPOTS = int(sys.argv[3])
    print(" ---> IMG NUMBER: {}".format(IMG_NUMBER))
    print(" ---> SPOT SIZE: {} pix".format(SPOT_SIZE))
    print(" ---> EVALUATING IMAGE WITH {} SPOTS".format(NUMBER_OF_SPOTS))
else:
    raise ValueError("---> ERROR: Please introduce the argument IMG NUMBER, SPOT_SIZE, and_NUMBER_OF_SPOTS")

img_dir = fn.image_list(IMG_DIR)[IMG_NUMBER]
print(" ---> IMAGE DIRECTORY: {}".format(img_dir))

# Solve LIS adding extra points and save data.

NUMBER_OF_LIS_EXECUTIONS = 20

tm1 = time.time()
full_data = []
for nmbr_of_exec in range(NUMBER_OF_LIS_EXECUTIONS):
    try:
        print(' ---> Execution number: {} - Size: {} - N spots: {}'.format(nmbr_of_exec + 1, SPOT_SIZE,
                                                                           NUMBER_OF_SPOTS))
        solution = stt.solve_lis(img_dir, NUMBER_OF_SPOTS, SPOT_SIZE)
        dt_lis = [1]
        dt_lis.extend(solution)
    except Exception as err:
        dt_lis = fn.zero_data()
        print(" ---> ERROR: {}".format(err))
    full_data.append(dt_lis)
# Compute statistical result
status_column = [row[0] for row in full_data]
lis_mean = (sum(status_column) / len(status_column)) * 100
final_result = [lis_mean, 0, 0, 0, 0, 0]
full_data.append(final_result)
# Save data
df = pd.DataFrame(data=full_data)
df.columns = ["STATUS", "RA", "DEC", "Roll", "sig", "Nr"]
fname = "{}/Measurements/pic_{}/spots/s{}_np{}.csv".format(SCRIPT_DIR, IMG_NUMBER + 1, SPOT_SIZE, NUMBER_OF_SPOTS)
df.to_csv(fname, index=False)
tm2 = time.time()
print("\n ---> TOTAL SCRIPT TIME: {:.3f}\n".format(tm2 - tm1))
sys.exit()
