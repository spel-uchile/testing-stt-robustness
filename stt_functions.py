import multiprocessing
import numpy as np
import os
import platform
import random
import re
import subprocess
from astropy.io import fits, ascii
from astropy.table import Table
from PIL import Image

# CONSTANT VALUES.
x_pix = 512
y_pix = 512
pix2mm = 0.002695
focal_len_mm = 3.04
# DIRECTORIES.
REPO_PATH = "/REPO_PATH"
DIR_STT_DATA = "{}/testing-stt-robustness/stt_data".format(REPO_PATH)
DIR_stars = '{}/testing-stt-robustness/stt_data/sext'.format(REPO_PATH)
DIR_IMG_FITS = "{}/testing-stt-robustness/stt_data/img.fits".format(REPO_PATH)
DIR_proj_cat2 = '{}/RA_DEC_V2/Proyectado/cat_RA_'.format(REPO_PATH)
dir_normal_cat = '{}/RA_DEC_V2/Normal/'.format(REPO_PATH)
new_proj_cat = '{}/testing-stt-robustness/stt_data/new_cat'.format(REPO_PATH)
DIR_stars_cp = '{}/testing-stt-robustness/stt_data/sext_original'.format(REPO_PATH)
# MATCH PARAMETERS.
param1 = 'trirad=0.002 nobj=15 max_iter=1 matchrad=0.1 scale=1'
param2 = 'trirad=0.002 nobj=20 max_iter=5 matchrad=0.01 scale=1'
# REGULAR EXPRESSIONS.
match_std_search = re.compile(r"sig=(-*\d\.\d+e...) Nr=(-*\d+) Nm=(-*\d+) sx=(-*\d\.\d+e...) sy=(-*\d\.\d+e...)")
match_numbers_search = re.compile(r"a=(-*\d\.\d+e...) b=(-*\d\.\d+e...) c=(-*\d\.\d+e...) "
                                  r"d=(-*\d\.\d+e...) e=(-*\d\.\d+e...) f=(-*\d\.\d+e...)")
# Another relevant parameters
LIS_MAX_ITER = 5
SEXTRACTOR_MAX_STARS = 40
sextractor_str = "sextractor"


def jpg2fits(full_dir_img, fits_name_of_image):
    """
    Generate .fits from .jpg image.
    """
    image = Image.open(full_dir_img)
    image_bw = image.convert('L')
    x_size, y_size = image_bw.size
    fits1 = image_bw.getdata()
    if platform.machine().endswith('64'):
        fits2 = np.array(fits1, dtype=np.int32)
    else:
        fits2 = np.array(fits1)
    fits3 = fits2.reshape(y_size, x_size)
    fits4 = np.flipud(fits3)
    fits5 = fits.PrimaryHDU(data=fits4)
    fits5.writeto(fits_name_of_image, overwrite=True)
    return 0


def jpg2fits_random_points(full_dir_img, fits_name_of_image, n_points, point_size):
    """
    Generate .fits from .jpg image and add an specific number of random points to it.
    """
    image = Image.open(full_dir_img)
    image_bw = image.convert('L')
    x_size, y_size = image_bw.size
    fits1 = image_bw.getdata()
    if platform.machine().endswith('64'):
        fits2 = np.array(fits1, dtype=np.int32)
    else:
        fits2 = np.array(fits1)
    fits3 = fits2.reshape(y_size, x_size)
    fits4 = np.flipud(fits3)
    fits5 = add_points_to_img(fits4, n_points, point_size)
    fits6 = fits.PrimaryHDU(data=fits5)
    fits6.writeto(fits_name_of_image, overwrite=True)
    return 0


def jpg2fits_random_lines(full_dir_img, fits_name_of_image, n_points, point_size):
    """
    Generate .fits from .jpg image and add an specific number of random lines to it.
    """
    image = Image.open(full_dir_img)
    image_bw = image.convert('L')
    x_size, y_size = image_bw.size
    fits1 = image_bw.getdata()
    if platform.machine().endswith('64'):
        fits2 = np.array(fits1, dtype=np.int32)
    else:
        fits2 = np.array(fits1)
    fits3 = fits2.reshape(y_size, x_size)
    fits4 = np.flipud(fits3)
    fits5 = add_lines_to_img(fits4, n_points, point_size)
    fits6 = fits.PrimaryHDU(data=fits5)
    fits6.writeto(fits_name_of_image, overwrite=True)
    return 0


def add_points_to_img(array, n_points, size):
    for ii in range(n_points):
        rand_nmb_x = random.randint(10, 1013)
        rand_nmb_y = random.randint(10, 1013)
        if size == 1:
            array[rand_nmb_x, rand_nmb_y] = 255
        elif size == 2:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
        elif size == 3:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
        elif size == 4:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
        elif size == 5:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
        elif size == 6:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
            array[rand_nmb_x - 1, rand_nmb_y] = 255
        elif size == 7:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
            array[rand_nmb_x - 1, rand_nmb_y] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 1] = 255
        elif size == 8:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
            array[rand_nmb_x - 1, rand_nmb_y] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 2] = 255
        elif size == 9:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
            array[rand_nmb_x - 1, rand_nmb_y] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 2] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 2] = 255
        elif size == 10:
            array[rand_nmb_x, rand_nmb_y] = 255
            array[rand_nmb_x + 1, rand_nmb_y] = 255
            array[rand_nmb_x, rand_nmb_y + 1] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x, rand_nmb_y + 2] = 255
            array[rand_nmb_x - 1, rand_nmb_y] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 1] = 255
            array[rand_nmb_x - 1, rand_nmb_y + 2] = 255
            array[rand_nmb_x + 1, rand_nmb_y + 2] = 255
            array[rand_nmb_x, rand_nmb_y + 3] = 255
        else:
            array = 0
    return array


def add_lines_to_img(array, n_points, size):
    for ii in range(n_points):
        rand_nmb_x = random.randint(10, 1013)
        if size == 1:
            array[rand_nmb_x, :] = 255
        elif size == 2:
            array[rand_nmb_x, :] = 255
            array[rand_nmb_x + 1, :] = 255
        elif size == 3:
            array[rand_nmb_x, :] = 255
            array[rand_nmb_x + 1, :] = 255
            array[rand_nmb_x - 1, :] = 255
        elif size == 4:
            array[rand_nmb_x, :] = 255
            array[rand_nmb_x + 1, :] = 255
            array[rand_nmb_x - 1, :] = 255
            array[rand_nmb_x + 2, :] = 255
        elif size == 5:
            array[rand_nmb_x, :] = 255
            array[rand_nmb_x + 1, :] = 255
            array[rand_nmb_x + 2, :] = 255
            array[rand_nmb_x - 1, :] = 255
            array[rand_nmb_x - 2, :] = 255
        else:
            array = 0
    return array


def apply_sextractor(img_fits_name):
    """
    Apply Source Extractor over an image and generates a catalog.
    """
    os.chdir(DIR_STT_DATA)
    sext_task = "{} {}".format(sextractor_str, img_fits_name)
    subprocess.check_output(sext_task, shell=True)
    sext1 = ascii.read('./test.cat', format='sextractor')
    sext1.sort(['MAG_ISO'])
    sextractor_stars = SEXTRACTOR_MAX_STARS
    sext2 = sext1[0:sextractor_stars]
    sext_x_pix = sext2['X_IMAGE']
    sext_y_pix = sext2['Y_IMAGE']
    sext_mag = sext2['MAG_ISO']
    sext_x_mm = (sext_x_pix - x_pix) * pix2mm
    sext_y_mm = (sext_y_pix - y_pix) * pix2mm
    sext_filename = 'sext'
    ascii.write([sext_x_mm, sext_y_mm, sext_mag], sext_filename, delimiter=' ', format='no_header', overwrite=True,
                formats={'X': '% 15.10f', 'Y': '% 15.10f', 'Z': '% 15.10f'}, names=['X', 'Y', 'Z'])
    return sext_x_pix, sext_y_pix, sext_x_mm, sext_y_mm


def apply_sextractor_removing_stars(img_fits_name, n_removed_stars, star_type='BRIGHT'):
    """
    Apply Source Extractor over an image and generates a catalog.
    Also, remove an specific number of bright/dim stars.
    """
    sext_task = "{} {}".format(sextractor_str, img_fits_name)
    subprocess.check_output(sext_task, shell=True)
    sext1 = ascii.read('./test.cat', format='sextractor')
    sext1.sort(['MAG_ISO'])
    sextractor_stars = SEXTRACTOR_MAX_STARS
    if star_type == 'BRIGHT':
        sext2 = sext1[n_removed_stars:sextractor_stars]
    elif star_type == 'DIM':
        sext2 = sext1[0:n_removed_stars]
    else:
        print('--> ERROR!')
        sext2 = 0
    print(' --- STARS --- ')
    print(sext2)
    sext_x_pix = sext2['X_IMAGE']
    sext_y_pix = sext2['Y_IMAGE']
    sext_mag = sext2['MAG_ISO']
    sext_x_mm = (sext_x_pix - x_pix) * pix2mm
    sext_y_mm = (sext_y_pix - y_pix) * pix2mm
    sext_filename = 'sext'
    ascii.write([sext_x_mm, sext_y_mm, sext_mag], sext_filename, delimiter=' ', format='no_header', overwrite=True,
                formats={'X': '% 15.10f', 'Y': '% 15.10f', 'Z': '% 15.10f'}, names=['X', 'Y', 'Z'])
    return sext_x_pix, sext_y_pix, sext_x_mm, sext_y_mm


def get_catalog_center_points(x_center, y_center, distance):
    """
    Get the center points for different catalogs segments, for a given distance and starting point.
    It consider declination of center site.
    """
    catalog_center_list = list()
    for jj1 in range(y_center, 90, distance):
        aux1 = (1 / np.cos(np.deg2rad(jj1)))
        distance_ra1 = int(round(distance * aux1))
        for ii1 in range(x_center, 360, distance_ra1):
            catalog_center_list.append([ii1, jj1])
    for jj2 in range(y_center - distance, -90, -distance):
        aux2 = (1 / np.cos(np.deg2rad(jj2)))
        distance_ra2 = int(round(distance * aux2))
        for ii2 in range(x_center, 360, distance_ra2):
            catalog_center_list.append([ii2, jj2])
    catalog_center_list = catalog_center_list + [[0, 90], [0, -90]]
    return catalog_center_list


def call_match_list(ra_dec_list, base='catalog'):
    """
    Call 'Match' with RA/DEC list in the shell.
    """
    ra, dec = ra_dec_list
    if base == 'catalog':
        match = set_match_str(ra, dec, DIR_stars, DIR_proj_cat2, param1)
    elif base == 'picture':
        match = set_match_str(ra, dec, DIR_stars, DIR_proj_cat2, param1, base='picture')
    else:
        raise ValueError('==> ERROR: Select a valid base for Match!')
    status, result = subprocess.getstatusoutput(match)
    return status, result


def set_match_str(ra, dec, dir_stars, dir_proj_cat2, param, base='catalog'):
    """
    Define and set the 'match' string before calling it in the shell.
    """
    ra_dec_str = str(ra) + '_DEC_' + str(dec)
    if base == 'catalog':
        match_str = 'match ' + dir_stars + ' 0 1 2 ' + dir_proj_cat2 + ra_dec_str + ' 0 1 2 ' + param
    elif base == 'picture':
        match_str = 'match ' + dir_proj_cat2 + ra_dec_str + ' 0 1 2 ' + dir_stars + ' 0 1 2 ' + param
    else:
        raise ValueError('==> ERROR: Select a valid base for Match!')
    return match_str


def map_match_and_radec_list_multiprocess(ra_dec_list):
    """
    Apply 'call_match' multiprocessing depending on numbers of cores.
    """
    n_cores = multiprocessing.cpu_count()
    if n_cores == 1:
        first_match_results = map(call_match_list, ra_dec_list)
    else:
        pool = multiprocessing.Pool(n_cores)
        first_match_results = pool.map(call_match_list, ra_dec_list)
    return first_match_results


def get_table_with_matchs(ra_dec_list, first_match_results):
    """
    Select RA/DEC values in which a successful match was obtained, and generate a 'match' table.
    """
    match_table = Table(names=('RA_center', 'DEC_center', 'sig', 'Nr'))
    for i, (status, result) in enumerate(first_match_results):
        if status == 0:
            ra, dec = ra_dec_list[i]
            regexp_result = match_std_search.findall(result)[0]
            sig = float(regexp_result[0])
            nr = int(regexp_result[1])
            match_table.add_row([str(ra), str(dec), sig, nr])
    if len(match_table) == 0:
        print('--> After search in the whole catalog, I can not find any match between picture and catalog! :(')
        raise ValueError('There is no match ...')
    else:
        match_table.sort('Nr')
        match_table.reverse()
    return match_table


def get_match_candidates(match_table, n_candidates=3):
    """
    Select three (3) or five (5) 'match' candidates from 'first match table'.
    """
    len_table = len(match_table)
    if n_candidates == 3:
        if len_table == 1:
            match_candidates = match_table
        elif len_table == 2:
            match_candidates = match_table
        else:
            short_match_table = match_table[0:3]
            match_candidates = short_match_table
    elif n_candidates == 5:
        if len_table == 1:
            match_candidates = match_table
        elif len_table == 2:
            match_candidates = match_table
        elif len_table == 3:
            match_candidates = match_table
        elif len_table == 4:
            match_candidates = match_table
        else:
            short_match_table = match_table[0:5]
            match_candidates = short_match_table
    else:
        print('--> ERROR: The selected number of candidates is not programmed yet')
        match_candidates = 0
    return match_candidates


def get_first_match_data(candidates, try_n, base='catalog'):
    """
    With a list of 'match candidates', recall match to obtain the transformation relationship.
    """
    cand = candidates[try_n]
    if base == 'catalog':
        result = call_match_list([int(cand[0]), int(cand[1])])[1]
    elif base == 'picture':
        print('Base is picture!')
        result = call_match_list([int(cand[0]), int(cand[1])], base='picture')[1]
    else:
        raise ValueError('==> ERROR: Select a valid base for Match!')
    regexp_result_numbers = match_numbers_search.findall(result)[0]
    regexp_result_std = match_std_search.findall(result)[0]
    return regexp_result_numbers, regexp_result_std


def apply_match_trans(data):
    """
    Apply the 'match' transformation between picture and projected catalog.
    This is: center of picture (pix) ==> point in the projected catalog (mm).
    """
    match_a = float(data[0])
    match_b = float(data[1])
    match_c = float(data[2])
    match_d = float(data[3])
    match_ra_mm, match_dec_mm = match_a, match_d
    match_roll_rad = np.arctan2(match_c, match_b)
    match_roll_deg = np.rad2deg(match_roll_rad)
    return match_ra_mm, match_dec_mm, match_roll_deg


def plane2sky(ra_match_mm, dec_match_mm, ra_catalog, dec_catalog):
    """
    Deproject any arbitrary point in the camera. This is: mm ==> sky coordinates.
    """
    xi = ra_match_mm/float(focal_len_mm)
    eta = dec_match_mm/float(focal_len_mm)
    dec_catalog_rad = np.deg2rad(dec_catalog)
    arg1 = np.cos(dec_catalog_rad) - eta * np.sin(dec_catalog_rad)
    arg2 = np.arctan(xi / arg1)
    arg3 = np.sin(arg2)
    arg4 = eta * np.cos(dec_catalog_rad) + np.sin(dec_catalog_rad)
    alpha = ra_catalog + np.rad2deg(arg2)
    delta = np.rad2deg(np.arctan((arg3 * arg4) / xi))
    return alpha, delta


def search_catalog_objects(ra_first_match, dec_first_match):
    """
    Search in the normal catalog for all sky-objects (to the nearest catalog), and create a table.
    """
    ra_catalog = int(round(ra_first_match))
    dec_catalog = int(round(dec_first_match))
    new_cat_name = dir_normal_cat + 'cat_RA_' + str(int(ra_catalog)) + '_DEC_' + str(int(dec_catalog))
    new_cat = ascii.read(new_cat_name)
    noproj_table = Table([[], [], []])
    for ii in range(len(new_cat)):
        noproj_table.add_row([new_cat[ii][0], new_cat[ii][1], new_cat[ii][2]])
    return noproj_table


def sky2plane(star_list, ra_project_point, dec_project_point):
    """
    With a list of 'matched' stars, project all in the tangent plane.
    """
    cat_projected = Table([[], [], []])
    stars_len = len(star_list)
    for index in range(stars_len):
        alpha_deg = star_list[index][0]
        delta_deg = star_list[index][1]
        mag = star_list[index][2]
        alpha_rad = np.deg2rad(alpha_deg)
        delta_rad = np.deg2rad(delta_deg)
        alpha_0_rad = np.deg2rad(ra_project_point)
        delta_0_rad = np.deg2rad(dec_project_point)
        xi_up = np.cos(delta_rad) * np.sin(alpha_rad - alpha_0_rad)
        xi_down = np.sin(delta_0_rad) * np.sin(delta_rad)\
            + np.cos(delta_0_rad) * np.cos(delta_rad) * np.cos(alpha_rad - alpha_0_rad)
        xi = xi_up/xi_down
        eta_up = np.cos(delta_0_rad) * np.sin(delta_rad)\
            - np.sin(delta_0_rad) * np.cos(delta_rad) * np.cos(alpha_rad - alpha_0_rad)
        eta_down = xi_down
        eta = eta_up / eta_down
        xi_mm = xi * focal_len_mm
        eta_mm = eta * focal_len_mm
        cat_projected.add_row([xi_mm, eta_mm, mag])
    cat_name = 'new_cat'
    ascii.write(cat_projected, cat_name, delimiter=' ', format='no_header', overwrite=True,
                formats={'X': '% 15.5f', 'Y': '% 15.5f', 'Z': '% 15.2f'}, names=['X', 'Y', 'Z'])
    return 0


def call_match_once(base='catalog', outfile=None):
    """
    Call 'Match' one time, in further 'match' iterations.
    """
    if base == 'catalog':
        match_str = 'match ' + DIR_stars + ' 0 1 2 ' + new_proj_cat + ' 0 1 2 ' + param2
    elif base == 'picture':
        match_str = 'match ' + new_proj_cat + ' 0 1 2 ' + DIR_stars + ' 0 1 2 ' + param2
    else:
        raise ValueError('==> ERROR: Select a valid base for Match!')
    # print(match_str)
    if outfile is not None:
        match_str = match_str + ' outfile=' + outfile
    status, result = subprocess.getstatusoutput(match_str)
    # print('Status: ', status)
    # print('Result: ', result)
    regexp_result_numbers = match_numbers_search.findall(result)[0]
    regexp_result_std = match_std_search.findall(result)[0]
    return regexp_result_numbers, regexp_result_std


def normalize_coord(ra, roll):
    """
    Set the RA and Roll coordinates in a standard way (like STEREO way!).
    """
    if ra > 180.0:
        ra_out = ra - 360.0
    else:
        ra_out = ra
    if roll < 0.0:
        roll_out = roll + 180.0
    else:
        roll_out = roll - 180.0
    return ra_out, roll_out


def search_for_matches():
    """
    Search for matched objects.
    """
    catalog_list = ascii.read('./matched.mtB')
    stars_list = ascii.read('./matched.mtA')
    catalog_table = Table([[], []])
    stars_table = Table([[], []])
    for ii in range(len(catalog_list)):
        catalog_table.add_row([catalog_list[ii][1], catalog_list[ii][2]])
        stars_table.add_row([stars_list[ii][1], stars_list[ii][2]])
    return catalog_table, stars_table


def get_match_diff(list_catalog_mm, list_stars_mm, unit='mm'):
    """
    Get differences between matched pairs (picture-catalog).
    """
    diff_list = list()
    for ii in range(len(list_catalog_mm)):
        x_diff = list_catalog_mm[ii][0] - list_stars_mm[ii][0]
        y_diff = list_catalog_mm[ii][1] - list_stars_mm[ii][1]
        if unit == 'mm':
            diff_list.append([x_diff, y_diff])
        elif unit == 'pix':
            x_diff_pix = float(x_diff) / pix2mm
            y_diff_pix = float(y_diff) / pix2mm
            diff_list.append([x_diff_pix, y_diff_pix])
        else:
            raise ValueError('--> ERROR: Please insert a valid measurement unit!')
    return diff_list


def solve_lis(img_full_dir, random_points=0, point_size=0, stain_type="spot"):
    """
    Solve the Lost-In-Space problem, in a SIMPLE way
    """
    # Apply SExtractor.
    if random_points == 0:
        jpg2fits(img_full_dir, DIR_IMG_FITS)
    else:
        if stain_type == "spot":
            jpg2fits_random_points(img_full_dir, DIR_IMG_FITS, random_points, point_size)
        elif stain_type == "lines":
            jpg2fits_random_lines(img_full_dir, DIR_IMG_FITS, random_points, point_size)
    sext_x_pix, sext_y_pix, sext_x_mm, sext_y_mm = apply_sextractor(DIR_IMG_FITS)
    # Apply Match - First iteration.
    ra_dec_list = get_catalog_center_points(0, 0, 5)
    first_match_map_results = map_match_and_radec_list_multiprocess(ra_dec_list)
    first_match_table = get_table_with_matchs(ra_dec_list, first_match_map_results)
    match_candidates = get_match_candidates(first_match_table, n_candidates=5)
    print(match_candidates)
    attempts = 0
    while attempts < LIS_MAX_ITER:
        try:
            first_ra_catalog, first_dec_catalog = match_candidates[attempts][0], match_candidates[attempts][1]
            print('{} {} {}'.format(attempts, first_ra_catalog, first_dec_catalog))
            first_match_data, first_match_std = get_first_match_data(match_candidates, attempts)
            first_ra_mm, first_dec_mm, first_roll_deg = apply_match_trans(first_match_data)
            first_alpha, first_delta = plane2sky(first_ra_mm, first_dec_mm, first_ra_catalog, first_dec_catalog)
            catalog_matches1, stars_matches1 = search_for_matches()
            # Apply Match - Second and third iteration.
            noproj_table = search_catalog_objects(first_alpha, first_delta)
            sky2plane(noproj_table, first_alpha, first_delta)
            second_match_data, second_match_std = call_match_once()
            second_ra_mm, second_dec_mm, second_roll_deg = apply_match_trans(second_match_data)
            second_alpha, second_delta = plane2sky(second_ra_mm, second_dec_mm, first_alpha, first_delta)
            sky2plane(noproj_table, second_alpha, second_delta)
            third_match_data, third_match_std = call_match_once()
            third_ra_mm, third_dec_mm, third_roll_deg = apply_match_trans(third_match_data)
            third_alpha, third_delta = plane2sky(third_ra_mm, third_dec_mm, second_alpha, second_delta)
            break
        except Exception as err:
            attempts += 1
            print('---> ERROR: {}'.format(err))
    if attempts == LIS_MAX_ITER:
        raise ValueError('--> ERROR: After five attempts to find a match, it can not be done :(')
    # Print final results.
    print("===> SOLUTION: RA = {:.4f} / DEC = {:.4f} / Roll = {:.4f}".format(third_alpha, third_delta, third_roll_deg))
    return third_alpha, third_delta, third_roll_deg, third_match_std[0], third_match_std[1]


def img2fits_and_sext(img_full_dir):
    """
    Transform .jpg image to .fits and apply SExtractor over it.
    It save a copy of the original 'sext' catalog.
    """
    jpg2fits(img_full_dir, DIR_IMG_FITS)
    _, _, _, _, = apply_sextractor(DIR_IMG_FITS)
    task = "cp {} {}".format(DIR_stars, DIR_stars_cp)
    status = subprocess.call(task, shell=True)
    return status


def solve_lis_removing_stars(removed_stars=1, star_type='BRIGHT'):
    """
    Solve the Lost-In-Space problem, removing specific stars from file sext_original and save it in file sext
    """
    # Removing stars
    with open(DIR_stars_cp) as file:
        lines = file.readlines()
        lines = [line.rstrip() for line in lines]
    sextractor_stars = SEXTRACTOR_MAX_STARS
    if star_type == 'BRIGHT':
        sext = lines[removed_stars:sextractor_stars]
    elif star_type == 'DIM':
        aux_stars = sextractor_stars - removed_stars
        sext = lines[0:aux_stars]
    else:
        print('--> ERROR!')
        sext = 0
    with open(DIR_stars, 'w') as f:
        for item in sext:
            f.write("%s\n" % item)
        print("---> sext FILE LEN: {}".format(len(sext)))
        f.flush()
    # Apply Match - First iteration.
    ra_dec_list = get_catalog_center_points(0, 0, 5)
    first_match_map_results = map_match_and_radec_list_multiprocess(ra_dec_list)
    first_match_table = get_table_with_matchs(ra_dec_list, first_match_map_results)
    match_candidates = get_match_candidates(first_match_table, n_candidates=5)
    print(match_candidates)
    attempts = 0
    while attempts < LIS_MAX_ITER:
        try:
            first_ra_catalog, first_dec_catalog = match_candidates[attempts][0], match_candidates[attempts][1]
            print('{} {} {}'.format(attempts, first_ra_catalog, first_dec_catalog))
            first_match_data, first_match_std = get_first_match_data(match_candidates, attempts)
            first_ra_mm, first_dec_mm, first_roll_deg = apply_match_trans(first_match_data)
            first_alpha, first_delta = plane2sky(first_ra_mm, first_dec_mm, first_ra_catalog, first_dec_catalog)
            catalog_matches1, stars_matches1 = search_for_matches()
            diff_list_match1 = get_match_diff(catalog_matches1, stars_matches1, unit='pix')
            # Apply Match - Second and third iteration.
            noproj_table = search_catalog_objects(first_alpha, first_delta)
            sky2plane(noproj_table, first_alpha, first_delta)
            second_match_data, second_match_std = call_match_once()
            second_ra_mm, second_dec_mm, second_roll_deg = apply_match_trans(second_match_data)
            second_alpha, second_delta = plane2sky(second_ra_mm, second_dec_mm, first_alpha, first_delta)
            sky2plane(noproj_table, second_alpha, second_delta)
            third_match_data, third_match_std = call_match_once()
            third_ra_mm, third_dec_mm, third_roll_deg = apply_match_trans(third_match_data)
            third_alpha, third_delta = plane2sky(third_ra_mm, third_dec_mm, second_alpha, second_delta)
            break
        except Exception as err:
            attempts += 1
            print('---> ERROR: {}'.format(err))
    if attempts == LIS_MAX_ITER:
        raise ValueError('--> ERROR: After five attempts to find a match, it can not be done :(')
    # Print final results.
    print("===> SOLUTION: RA = {:.4f} / DEC = {:.4f} / Roll = {:.4f}".format(third_alpha, third_delta,
                                                                                 third_roll_deg))
    return third_alpha, third_delta, third_roll_deg, third_match_std[0], third_match_std[1]
