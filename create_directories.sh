#!/bin/bash

mkdir Measurements
cd Measurements

for i in {1..50}
do
  echo $i
  mkdir pic_$i
  cd pic_$i
  mkdir spots
  mkdir lines
  mkdir stars
  cd ../
  echo ---
done
