# Testing Star Tracker algorithm robustness

## Introduction

This GitLab offers code to test a Star Tracker algorithm's robustness under 
perturbations from space radiation. Specifically, the STT code comes 
from [SOST GitHub webpage.](https://github.com/spel-uchile/Star_Tracker)

The radiation evaluation over the image is done in three ways:
- Adding dots
- Adding lines
- Removing extracted stars 

## Execution instructions

### 1.- Dependencies

The STT algorithm is based on _Python_ _3.X_. The following libraries are imported when 
using this code. You need to install them to properly operate with the algorithm:
- astropy
- multiprocessing
- numpy
- natsort
- os
- pandas
- PIL
- platform
- random
- re
- subprocess
- sys
- time

### 2.- Initialization

The STT algorithm needs the astronomical tools _Source_ _Extractor_ and _Match_ to 
work properly. Install them by executing:
```
./stt_installer.sh
```

Then, execute __create_directories.sh__ to create folders where the computed data 
will be saved:
```
./create_directories.sh
```

You will also need to change the variable __REPO_PATH__ by the specific path in which 
you run this script. To know your current path, you can execute:
```
pwd
```

The variable __REPO_PATH__ is in the following scripts:
- compute_spots.sh (line 13)
- compute_lines.sh (line 13)
- compute_remove_stars.sh (line 7)
- OneImgAnlsys-SolveLISPandas-Spots.py (line 15)
- OneImgAnlsys-SolveLISPandas-Lines.py (line 15)
- OneImgAnlsys-SolveLISPandas-Remove_stars.py (line 17)
- stt_functions.py (line 18)

### 3.- Running the experiments

A.- To run the experiment of adding white spots, execute:
```
./compute_spots.sh
```

B.- To run the experiment of adding white lines, execute:
```
./compute_lines.sh
```

C.- To run the experiment of removing extracted objects, execute:
```
./compute_remove_stars.sh
```

## Questions?
<p>Author: Samuel T. Gutiérrez.<br>
Ph.D.(c) in Electrical Engineering at the University of Chile, Santiago, Chile.<br>
Contact: samuel.gutierrez@ug.uchile.cl<br>

README updated on April 14, 2023.</p>

