import os
from natsort import natsorted


def image_list(img_directory):
    img_list = ["{}/{}".format(img_directory, f) for f in os.listdir(img_directory)
                if os.path.isfile(os.path.join(img_directory, f)) if f.endswith(".jpg")]
    sort_list = natsorted(img_list)
    return sort_list


def zero_data():
    zdata = [0, 0, 0, 0, 0, 0]
    return zdata
